package com.toptal.kcalmeter.web.rest;

import com.google.gson.*;
import com.toptal.kcalmeter.Application;
import com.toptal.kcalmeter.repository.EntryRepository;
import com.toptal.kcalmeter.service.EntryService;
import com.toptal.kcalmeter.web.rest.dto.EntryDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.inject.Inject;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/**
 * Test class for the EntryResource REST controller.
 *
 * @see EntryResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class EntryResourceIntTest {

    @Inject
    private EntryRepository entryRepository;

    @Inject
    private EntryService entryService;

    private MockMvc restEntryMockMvc;

    private Gson gson = new GsonBuilder()
        .registerTypeAdapter(LocalDateTime.class, new JsonSerializer<LocalDateTime>() {

            @Override
            public JsonElement serialize(LocalDateTime src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            }
        })
        .create();

    private EntryDto generateEntryDto(LocalDateTime time, Integer calVal, String text, Long userId) {
        EntryDto entry = new EntryDto();
        entry.setEntryDate(time != null ? time : LocalDateTime.now());
        entry.setCalValue(calVal != null ? calVal : 100);
        entry.setText(text != null ? text : "meal");
        entry.setUserId(userId != null ? userId : 1L);
        return entry;
    }

    @Before
    public void setup() {
        EntryResource entryResource = new EntryResource();
        ReflectionTestUtils.setField(entryResource, "entryRepository", entryRepository);
        ReflectionTestUtils.setField(entryResource, "entryService", entryService);
        this.restEntryMockMvc = MockMvcBuilders.standaloneSetup(entryResource).build();
//        restEntryMockMvc.perform(post("/api//");
    }

//    @Test
//    public void testAddAndGet() throws Exception {
//
//        restEntryMockMvc.perform(post("/api/entry/")
////            .with(csrf())
//            .with(user("user").password("user").roles("USER"))
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(gson.toJson(generateEntryDto(null, null, "test", null)))
//            .accept(MediaType.APPLICATION_JSON))
//            .andExpect(status().isOk())
//            .andExpect(jsonPath("$.text").value("test"));
//    }
////
    @Test
    public void testGetUnknownUser() throws Exception {
//        restUserMockMvc.perform(get("/api/users/unknown")
//            .accept(MediaType.APPLICATION_JSON))
//            .andExpect(status().isNotFound());
    }
}
