'use strict';

describe('Controllers Tests ', function() {

    beforeEach(mockApiAccountCall);
    beforeEach(mockI18nCalls);

    describe('SettingsController', function() {

        var $scope, $q; // actual implementations
        var MockPrincipal, MockAuth; // mocks
        var createController; // local utility functions

        beforeEach(inject(function($injector) {
            $q = $injector.get('$q');
            $scope = $injector.get("$rootScope").$new();
            MockAuth = jasmine.createSpyObj('MockAuth', ['updateAccount']);
            MockPrincipal = jasmine.createSpyObj('MockPrincipal', ['identity']);
            var locals = {
                '$scope': $scope,
                'Principal': MockPrincipal,
                'Auth': MockAuth
            };
            createController = function() {
                $injector.get('$controller')('EntriesController', locals);
            }
        }));

        it('should clear filter', function() {
            //GIVEN
            $scope.$apply(createController);
            $scope.filter = {
                userId: 1,
                dateFrom: '2010-10-10',
                dateTo: '2010-10-10',
                timeTo: '10:10',
                timeFrom: '10:10'
            };

            //WHEN
            $scope.clearFilter();

            //THEN
            expect($scope.filter.userId).toBeNull();
            expect($scope.filter.dateFrom).toBeNull();
            expect($scope.filter.dateTo).toBeNull();
            expect($scope.filter.timeTo).toBeNull();
            expect($scope.filter.timeFrom).toBeNull();

        });


    });
});
