package com.toptal.kcalmeter.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.toptal.kcalmeter.domain.Entry;
import com.toptal.kcalmeter.domain.User;
import com.toptal.kcalmeter.repository.EntryRepository;
import com.toptal.kcalmeter.repository.UserRepository;
import com.toptal.kcalmeter.security.AuthoritiesConstants;
import com.toptal.kcalmeter.service.EntryService;
import com.toptal.kcalmeter.service.UserService;
import com.toptal.kcalmeter.web.rest.dto.EntryDto;
import com.toptal.kcalmeter.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller for managing entries.
 */
@RestController
@RequestMapping("/api")
public class EntryResource {

    private final Logger log = LoggerFactory.getLogger(EntryResource.class);

    @Inject
    private EntryRepository entryRepository;
    @Inject
    private UserRepository userRepository;
    @Inject
    private UserService userService;
    @Inject
    private EntryService entryService;

    /**
     * POST  /entry -> Create a new Entry.
     */
    @RequestMapping(value = "/entry",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured({AuthoritiesConstants.APP_ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<Entry> createEntry(@RequestBody EntryDto entry) throws URISyntaxException {
        log.debug("REST request to save entry : {}", entry);
        User user = userService.getUserWithAuthorities();
        if (entry.getUserId() == null ||
            !user.getAuthorities().stream().anyMatch(item -> item.getName().equals(AuthoritiesConstants.APP_ADMIN))){
            entry.setUserId(user.getId());
        }
        if (entry.getEntryDate() == null) {
            entry.setEntryDate(LocalDateTime.now());
        }
        if (entry.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new entry cannot already have an ID").body(null);
        }
        Entry result = entryRepository.save(entry.toEntity());
        return ResponseEntity.created(new URI("/api/entry/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("entry", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /entry -> Updates an existing Entry.
     */
    @RequestMapping(value = "/entry",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @Secured({AuthoritiesConstants.APP_ADMIN, AuthoritiesConstants.USER})
    public ResponseEntity<EntryDto> updateEntry(@RequestBody EntryDto newEntry) throws URISyntaxException {
        log.debug("REST request to update entry : {}", newEntry);
        User user = userService.getUserWithAuthorities();
        if (newEntry.getEntryDate() == null) {
            newEntry.setEntryDate(LocalDateTime.now());
        }
        if (newEntry.getUserId() == null||
            !user.getAuthorities().stream().anyMatch(item -> item.getName().equals(AuthoritiesConstants.APP_ADMIN))) {
            newEntry.setUserId(user.getId());
        }

        return entryRepository
            .findOneById(newEntry.getId())
            .map(entry -> {
                entry.setText(newEntry.getText());
                entry.setCalValue(newEntry.getCalValue());
                entry.setEntryDate(newEntry.getEntryDate());
                entry.setUser(userRepository.getOne(newEntry.getUserId()));
                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("entry", newEntry.getId().toString()))
                    .body(new EntryDto(entryRepository
                        .findOne(newEntry.getId())));
            })
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * GET  /entry -> get all entries + filters.
     */
    @RequestMapping(value = "/entry",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<EntryDto>> getAllEntries(@RequestParam(name = "date", required = false) String date,
                                                        @RequestParam(name = "dateFrom", required = false) String dateFrom,
                                                        @RequestParam(name = "dateTo", required = false) String dateTo,
                                                        @RequestParam(name = "timeFrom", required = false) String timeFrom,
                                                        @RequestParam(name = "timeTo", required = false) String timeTo ,
                                                        @RequestParam(name = "userId", required = false) Long userId)
        throws URISyntaxException, ParseException {
        // TODO add filters
        if (userId == null) {
            userId = userService.getUserWithAuthorities().getId();
        }
        List<Entry> entryList = new ArrayList<>();
        if (date != null) {
            entryList = entryService
                .getByDate(LocalDate.parse(date, DateTimeFormatter.ISO_DATE),
                    userId);
        } else
        if (dateFrom != null || dateTo != null || timeFrom != null || timeTo != null) {
            entryList = entryService
                .getByDate(dateFrom != null ? LocalDate.parse(dateFrom, DateTimeFormatter.ISO_DATE) : null,
                    dateTo != null ? LocalDate.parse(dateTo, DateTimeFormatter.ISO_DATE) : null,
                    timeFrom == null ? null : LocalTime.parse(timeFrom, DateTimeFormatter.ISO_TIME),
                    timeTo == null ? null : LocalTime.parse(timeTo, DateTimeFormatter.ISO_TIME),
                    userId);
        } else {
            entryList = entryRepository
                .findByUserIdOrderByEntryDateDesc(userId);
        }
        List<EntryDto> entryDtoList = entryList.stream()
            .map(EntryDto::new)
            .collect(Collectors.toList());

        return new ResponseEntity<>(entryDtoList, HttpStatus.OK);
    }

    /**
     * DELETE  /entry/{id} -> delete entry.
     */
    @RequestMapping(value = "entry/{id}",
        method = RequestMethod.DELETE)
    @Timed
    public void deleteEntry(@PathVariable Long id) {
        log.debug("REST request to delete Entry : {}", id);
        entryRepository.delete(entryRepository.findOne(id));
    }

    /**
     * GET  /entry/{id} -> get entry by id.
     */
    @RequestMapping(value = "entry/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EntryDto> getEntryById(@PathVariable Long id) {
        log.debug("REST request to get Entry with id : {}", id);
        return entryRepository
            .findOneById(id)
            .map(entry ->
                    ResponseEntity.ok()
                        .headers(HeaderUtil.createEntityUpdateAlert("entry", id.toString()))
                        .body(new EntryDto(entry))
            )
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }
}
