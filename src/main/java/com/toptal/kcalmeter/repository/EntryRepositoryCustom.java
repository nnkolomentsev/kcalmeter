package com.toptal.kcalmeter.repository;

import com.toptal.kcalmeter.domain.Entry;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Spring Data custom JPA repository for the Entry entity.
 */
public interface EntryRepositoryCustom {
    List<Entry> getByDates(LocalDateTime dateFrom, LocalDateTime dateTo, Long userId);
}
