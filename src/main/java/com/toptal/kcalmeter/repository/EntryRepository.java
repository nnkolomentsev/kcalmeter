package com.toptal.kcalmeter.repository;

import com.toptal.kcalmeter.domain.Entry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Entry entity.
 */
public interface EntryRepository extends JpaRepository<Entry, Long>, EntryRepositoryCustom {

    List<Entry> findAll();

    List<Entry> findByUserIdOrderByEntryDateDesc(Long userId);

    Optional<Entry> findOneById(Long entryId);

    @Override
    void delete(Entry t);

}
