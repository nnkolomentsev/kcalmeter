'use strict';
var transformReq = function (data, headers) {
    data = angular.copy(data);
    if (data.entryDate) {
        data.entryDate = moment(data.entryDate).format('YYYY-MM-DDTHH:mm:ss')
    }
    if(data.user){
        data.userId = data.user.id;
    }
    return angular.toJson(data);
};

angular.module('kkalMeterApp')
    .factory('Entry', function ($resource) {
        return $resource('api/entry/:id', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                //transformResponse: function (data) {
                //    data = angular.fromJson(data);
                //    return data;
                //}
            },
            'update': {method: 'PUT', url: 'api/entry/', transformRequest: function(data, headers){
                return transformReq(data,headers);
            }},
            'save': {method: 'POST', url: 'api/entry/', transformRequest: function(data, headers){
                return transformReq(data,headers);
            }}
        });
    });
